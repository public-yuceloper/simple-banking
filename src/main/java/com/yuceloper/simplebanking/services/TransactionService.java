package com.yuceloper.simplebanking.services;

import com.yuceloper.simplebanking.entities.DepositTransaction;
import com.yuceloper.simplebanking.entities.Transaction;
import com.yuceloper.simplebanking.entities.WithdrawalTransaction;
import com.yuceloper.simplebanking.repostories.TransactionRepository;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class TransactionService {
    private final TransactionRepository transactionRepository;

    public TransactionService(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    @Transactional
    public Transaction createDepositTransaction(double amount) {
        DepositTransaction depositTransaction = new DepositTransaction(amount);
        return transactionRepository.save(depositTransaction);
    }

    @Transactional
    public UUID createWithdrawalTransaction(double amount) {
        WithdrawalTransaction withdrawalTransaction = new WithdrawalTransaction(amount);
        WithdrawalTransaction savedTransaction = transactionRepository.save(withdrawalTransaction);
        return savedTransaction.getApprovalCode();
    }
}
