package com.yuceloper.simplebanking.services;

import com.yuceloper.simplebanking.entities.BankAccount;
import com.yuceloper.simplebanking.entities.DepositTransaction;
import com.yuceloper.simplebanking.entities.PhoneBillPaymentTransaction;
import com.yuceloper.simplebanking.entities.WithdrawalTransaction;
import com.yuceloper.simplebanking.repostories.BankAccountRepository;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

import javax.security.auth.login.AccountNotFoundException;
import java.util.UUID;

@Service
public class BankAccountService {
    private final BankAccountRepository bankAccountRepository;

    public BankAccountService(BankAccountRepository bankAccountRepository) {
        this.bankAccountRepository = bankAccountRepository;
    }

    public BankAccount hasAccount(String accountNumber){
        return bankAccountRepository.findByAccountNumber(accountNumber).orElse(null);
    }

    public BankAccount saveAccount(BankAccount bankAccount){
        return bankAccountRepository.save(bankAccount);
    }

    @Transactional
    public UUID creditAccount(String accountId, double amount) throws AccountNotFoundException {
        BankAccount account = bankAccountRepository.findByAccountNumber(accountId)
                .orElseThrow(() -> new AccountNotFoundException("Account not found"));
        account.credit(amount);
        var transaction = new DepositTransaction(amount);
        account.post(transaction);
        bankAccountRepository.save(account);
        return transaction.getApprovalCode();
    }

    @Transactional
    public UUID debitAccount(String accountId, double amount) throws AccountNotFoundException {
        BankAccount account = bankAccountRepository.findByAccountNumber(accountId)
                .orElseThrow(() -> new AccountNotFoundException("Account not found"));
        account.debit(amount);
        var transaction = new WithdrawalTransaction(amount);
        account.post(transaction);
        bankAccountRepository.save(account);
        return transaction.getApprovalCode();
    }

    @Transactional
    public UUID payPhoneBill(String accountId, String gsm, String phoneNumber, double amount) throws AccountNotFoundException {
        BankAccount account = bankAccountRepository.findByAccountNumber(accountId)
                .orElseThrow(() -> new AccountNotFoundException("Account not found"));
        account.debit(amount);
        var transaction = new PhoneBillPaymentTransaction(gsm, phoneNumber, amount);
        account.post(transaction);
        bankAccountRepository.save(account);
        return transaction.getApprovalCode();
    }

    public BankAccount getAccountByAccountNumber(String accountNumber) throws AccountNotFoundException {
        return bankAccountRepository.findByAccountNumber(accountNumber)
                .orElseThrow(() -> new AccountNotFoundException("Account not found"));
    }

    public double getAccountBalance(String accountNumber) throws AccountNotFoundException {
        var account = bankAccountRepository.findByAccountNumber(accountNumber)
                .orElseThrow(() -> new AccountNotFoundException("Account not found"));
        return account.getBalance();
    }
}