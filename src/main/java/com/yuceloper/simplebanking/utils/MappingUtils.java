package com.yuceloper.simplebanking.utils;

import com.yuceloper.simplebanking.entities.BankAccount;
import com.yuceloper.simplebanking.entities.Transaction;
import com.yuceloper.simplebanking.models.response.AccountResponse;
import com.yuceloper.simplebanking.models.response.TransactionResponse;

import java.util.List;
import java.util.stream.Collectors;

public class MappingUtils {

    public static AccountResponse mapToAccountResponse(BankAccount account) {
        List<TransactionResponse> transactionResponses = account.getTransactions().stream()
                .map(MappingUtils::mapToTransactionResponse)
                .collect(Collectors.toList());

        return new AccountResponse(
                account.getAccountNumber(),
                account.getOwner(),
                account.getBalance(),
                account.getCreateDate(),
                transactionResponses
        );
    }

    public static TransactionResponse mapToTransactionResponse(Transaction transaction) {
        return new TransactionResponse(
                transaction.getDate(),
                transaction.getAmount(),
                transaction.getClass().getSimpleName(),
                transaction.getApprovalCode()
        );
    }

}
