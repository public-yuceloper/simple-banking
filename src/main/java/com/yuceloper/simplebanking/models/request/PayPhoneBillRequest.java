package com.yuceloper.simplebanking.models.request;

import lombok.Data;

@Data
public class PayPhoneBillRequest {
    private String gsm;
    private String phoneNumber;
    private double amount;
}
