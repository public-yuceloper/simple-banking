package com.yuceloper.simplebanking.models.request;

import lombok.Data;

@Data
public class TransactionRequest {
    private double amount;
}
