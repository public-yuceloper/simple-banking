package com.yuceloper.simplebanking.models.response;


import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;
import java.util.UUID;

@Data
@AllArgsConstructor
public class TransactionResponse {
    private Date date;
    private double amount;
    private String type;
    private UUID approvalCode;
}