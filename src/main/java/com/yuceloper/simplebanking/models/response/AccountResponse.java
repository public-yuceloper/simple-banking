package com.yuceloper.simplebanking.models.response;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
public class AccountResponse {
    private String accountNumber;
    private String owner;
    private double balance;
    private Date createDate;
    private List<TransactionResponse> transactions;
}
