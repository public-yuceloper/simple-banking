package com.yuceloper.simplebanking.rest;

import com.yuceloper.simplebanking.entities.BankAccount;
import com.yuceloper.simplebanking.models.request.PayPhoneBillRequest;
import com.yuceloper.simplebanking.models.request.TransactionRequest;
import com.yuceloper.simplebanking.models.response.AccountResponse;
import com.yuceloper.simplebanking.models.response.ApiResponse;
import com.yuceloper.simplebanking.services.BankAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.security.auth.login.AccountNotFoundException;
import java.util.UUID;

import static com.yuceloper.simplebanking.utils.MappingUtils.mapToAccountResponse;

@RestController
@RequestMapping("/account/v1")
public class AccountController {

    private final BankAccountService bankAccountService;

    @Autowired
    public AccountController(BankAccountService bankAccountService) {
        this.bankAccountService = bankAccountService;
    }

    @PostMapping("/credit/{accountNumber}")
    public ResponseEntity<ApiResponse> creditAccount(
            @PathVariable String accountNumber,
            @RequestBody TransactionRequest transactionRequest) throws AccountNotFoundException {

            double amount = transactionRequest.getAmount();
            UUID approvalCode = bankAccountService.creditAccount(accountNumber, amount);
            ApiResponse response = new ApiResponse("OK", approvalCode.toString());
            return ResponseEntity.ok(response);
    }

    @PostMapping("/debit/{accountNumber}")
    public ResponseEntity<ApiResponse> debitAccount(
            @PathVariable String accountNumber,
            @RequestBody TransactionRequest transactionRequest) throws AccountNotFoundException {

        double amount = transactionRequest.getAmount();
        UUID approvalCode = bankAccountService.debitAccount(accountNumber, amount);
        ApiResponse response = new ApiResponse("OK", approvalCode.toString());
        return ResponseEntity.ok(response);
    }

    @PostMapping("/phone-bill/{accountNumber}")
    public ResponseEntity<ApiResponse> payPhoneBill(
            @PathVariable String accountNumber,
            @RequestBody PayPhoneBillRequest transactionRequest) throws AccountNotFoundException {

        double amount = transactionRequest.getAmount();
        UUID approvalCode = bankAccountService.payPhoneBill(accountNumber, transactionRequest.getGsm(), transactionRequest.getPhoneNumber(), amount);
        ApiResponse response = new ApiResponse("OK", approvalCode.toString());
        return ResponseEntity.ok(response);
    }

    @GetMapping("/{accountNumber}")
    public ResponseEntity<AccountResponse> getAccount(@PathVariable String accountNumber) throws AccountNotFoundException {
            BankAccount account = bankAccountService.getAccountByAccountNumber(accountNumber);
            AccountResponse response = mapToAccountResponse(account);
            return ResponseEntity.ok(response);
    }
}

