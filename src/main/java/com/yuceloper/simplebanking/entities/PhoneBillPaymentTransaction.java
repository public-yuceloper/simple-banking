package com.yuceloper.simplebanking.entities;

import jakarta.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.Date;

@Entity
@NoArgsConstructor
@AllArgsConstructor
public class PhoneBillPaymentTransaction extends Transaction {
    private String gsm;
    private String phoneNumber;

    public PhoneBillPaymentTransaction(String gsm, String phoneNumber, double amount) {
        this.setType("PhoneBillPaymentTransaction");
        this.setDate(new Date());
        this.setAmount(amount);
        this.gsm = gsm;
        this.phoneNumber = phoneNumber;
    }

    @Override
    public void processTransaction(BankAccount account) {
        account.credit(this.getAmount());
    }

}
