package com.yuceloper.simplebanking.entities;

import jakarta.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.Date;

@Entity
@NoArgsConstructor
public class WithdrawalTransaction extends Transaction {
    public WithdrawalTransaction(double amount) {
        this.setType("WithdrawalTransaction");
        this.setDate(new Date());
        this.setAmount(amount);
    }
    @Override
    public void processTransaction(BankAccount account) {
        account.credit(this.getAmount());
    }
}
