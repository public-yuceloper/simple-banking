package com.yuceloper.simplebanking.entities;

import com.yuceloper.simplebanking.exceptions.InsufficientFundsException;
import lombok.AllArgsConstructor;
import lombok.Data;
import jakarta.persistence.*;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class BankAccount {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String owner;
    private String accountNumber;
    private double balance;

    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate = new Date();

    @OneToMany(mappedBy = "bankAccount", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Transaction> transactions = new ArrayList<>();
    public BankAccount(String owner, String accountNumber) {
        this.owner = owner;
        this.accountNumber = accountNumber;
    }
    public BankAccount(String owner, int accountId) {
        this.owner = owner;
        this.accountNumber = String.valueOf(accountId);
    }
    public void credit(double amount) {
        balance += amount;
    }
    public void debit(double amount) {
        if (amount <= balance) {
            balance -= amount;
        } else {
            throw new InsufficientFundsException("Insufficient funds");
        }
    }
    public void post(Transaction transaction) {
        transaction.setBankAccount(this);
        transactions.add(transaction);
    }
}

