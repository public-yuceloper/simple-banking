package com.yuceloper.simplebanking.entities;

import jakarta.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.Date;

@Entity
@NoArgsConstructor
public class DepositTransaction extends Transaction {
    public DepositTransaction(double amount) {
        this.setType("DepositTransaction");
        this.setDate(new Date());
        this.setAmount(amount);
    }

    @Override
    public void processTransaction(BankAccount account) {
        account.credit(this.getAmount());
    }
}
