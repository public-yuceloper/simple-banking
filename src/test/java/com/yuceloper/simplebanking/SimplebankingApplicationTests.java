package com.yuceloper.simplebanking;

import com.yuceloper.simplebanking.entities.BankAccount;
import com.yuceloper.simplebanking.services.BankAccountService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.security.auth.login.AccountNotFoundException;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class SimplebankingApplicationTests {

    @Autowired
    private BankAccountService bankAccountService;

    private BankAccount account;

    @BeforeEach
    void setUp() {
        account = new BankAccount("Jim", 12345);
        //if(bankAccountService.hasAccount(account.getAccountNumber()) == null) bankAccountService.saveAccount(account);
    }

    @Test
    void testBankAccountOperations() throws AccountNotFoundException {
        bankAccountService.creditAccount(account.getAccountNumber(), 1000);
        bankAccountService.debitAccount(account.getAccountNumber(), 200);
        bankAccountService.payPhoneBill(account.getAccountNumber(), "Vodafone", "5423345566", 96.50);

        double expectedBalance = 703.50;
        assertEquals(expectedBalance, bankAccountService.getAccountBalance(account.getAccountNumber()), 0.0001);
    }

}
